ALTER TABLE vehicule ADD marque_ebs VARCHAR(100);
ALTER TABLE vehicule_aud ADD marque_ebs VARCHAR(100);
ALTER TABLE vehicule_aud ADD marqueebs_aud BOOLEAN;

create table poi_information (
    id                  bigserial primary key,
    external_id         varchar,
    name                varchar(100),
    operator_name       varchar(100),
    nerop_id            varchar(50),
    phone               varchar(20),
    public_state        varchar(50),
    poi_id              bigint references poi(id)
);
alter table poi_information owner to adm_ekolis;
grant all on poi_information to app_ekolis;
grant all on poi_information_id_seq to app_ekolis;
alter table poi_information add column created_at timestamp;
alter table poi_information add column updated_at timestamp;
alter table poi_information add column deleted boolean;

create table energy (
    id              bigserial primary key,
    external_id     varchar,
    name            varchar(100),
    kind            integer
);
alter table energy owner to adm_ekolis;
alter table energy add column created_at timestamp;
alter table energy add column updated_at timestamp;
alter table energy add column deleted boolean;
grant all on energy to app_ekolis;
grant all on energy_id_seq to app_ekolis;

create table energy_type (
    id                  bigserial primary key,
    external_id         varchar,
    name                varchar(100),
    product_reference   varchar(100),
    enabled             boolean,
    energy_id           bigint references energy(id)
);
alter table energy_type owner to adm_ekolis;
alter table energy_type add column created_at timestamp;
alter table energy_type add column updated_at timestamp;
alter table energy_type add column deleted boolean;
grant all on energy_type to app_ekolis;
grant all on energy_type_id_seq to app_ekolis;

create table tracteur_model (
    id                  bigserial primary key,
    external_id         varchar,
    name                varchar(100),
    brand               varchar(100),
    key                 varchar(100),
    tank_volume         integer,
    type_vehicle_name   varchar(100),
    main_energy_type_id bigint references energy_type(id),
    tracteur_id         bigint references tracteur_client(tracteur_id),
    enabled             boolean
);
alter table tracteur_model owner to adm_ekolis;
alter table tracteur_model add column created_at timestamp;
alter table tracteur_model add column updated_at timestamp;
alter table tracteur_model add column deleted boolean;
grant all on tracteur_model to app_ekolis;
grant all on tracteur_model_id_seq to app_ekolis;

create table tracteur_model_energy (
    id                  bigserial primary key,
    tractor_model_id    bigint,
    alt_energy_type_id  bigint
);
alter table tracteur_model_energy owner to adm_ekolis;
grant all on tracteur_model_energy to app_ekolis;
grant all on tracteur_model_energy_id_seq to app_ekolis;

create table dispenser (
    id              bigserial primary key,
    external_id     varchar,
    name            varchar(100),
    label           varchar(100),
    reference_name  varchar(100),
    enabled         boolean,
    connected       boolean,
    public_state    varchar(50),
    fueling_status  varchar(100),
    locked          boolean,
    wait_until      timestamp,
    station_id      bigint references poi_information(id),
    refill_id       bigint,
    vehicle_id      bigint
);
alter table dispenser owner to adm_ekolis;
alter table dispenser add column created_at timestamp;
alter table dispenser add column updated_at timestamp;
alter table dispenser add column deleted boolean;
grant all on dispenser to app_ekolis;
grant all on dispenser_id_seq to app_ekolis;

create table receptacle (
    id              bigserial primary key,
    external_id     varchar,
    dispenser_id    bigint references dispenser(id),
    energy_type_id  bigint references energy_type(id),
    state           varchar(50)
);
alter table receptacle owner to adm_ekolis;
alter table receptacle add column created_at timestamp;
alter table receptacle add column updated_at timestamp;
alter table receptacle add column deleted boolean;
grant all on receptacle to app_ekolis;
grant all on receptacle_id_seq to app_ekolis;

create table refill_state (
    id              bigserial primary key,
    pressure        int,
    temperature     int,
    soc             int,
    type            varchar(20)
);
alter table refill_state owner to adm_ekolis;
grant all on refill_state to app_ekolis;
grant all on refill_state_id_seq to app_ekolis;

create table refill_group (
    id                          bigserial primary key,
    external_id                 varchar,
    group_id                    varchar,
    attempts_count              int,
    real_attempts_count         int,
    pos                         varchar,
    start_refill                timestamp,
    end_refill                  timestamp,
    quantity                    double precision,
    start_state_id              bigint references refill_state(id),
    end_state_id                bigint references refill_state(id),
    currency                    varchar,
    taxe                        int,
    amount_HT                   double precision,
    amount                      double precision,
    vehicle_communication       boolean,
    vehicle_communication_lost  boolean,
    station_id                  bigint references poi_information(id),
    dispenser_id                bigint references dispenser(id),
    driver_id                   bigint,
    tracteur_model_id           bigint,
    capteur_id                  bigint,
    energy_id                   bigint,
    energy_type_id              bigint,
    customer_id                 bigint,
    enabled                     boolean,
    archived                    boolean
);
alter table refill_group owner to adm_ekolis;
alter table refill_group add column created_at timestamp;
alter table refill_group add column updated_at timestamp;
alter table refill_group add column deleted boolean;
grant all on refill_group to app_ekolis;
grant all on refill_group_id_seq to app_ekolis;
create index start_desc on refill_group(start_refill DESC);

create table device (
    id              bigserial primary key,
    external_id     varchar,
    country_code    varchar(10),
    phone_number    varchar(20),
    tracteur_id      bigint references tracteur_client(tracteur_id),
    driver_id       bigint references driver(driver_id)
);
alter table device owner to adm_ekolis;
alter table device add column created_at timestamp;
alter table device add column updated_at timestamp;
alter table device add column deleted boolean;
grant all on device to app_ekolis;
grant all on device_id_seq to app_ekolis;