const fs = require('fs');
const axios = require('axios')
const https = require('https');
const jose = require('node-jose');
const moment = require('moment');


class FillnDriveClient {

    constructor( endpoint, kid, login, token ) {

        this.endpoint = endpoint;
        this.kid = kid;
        this.login = login;
        this.token = token;

        // JWS KeyStore
        this.keystore = jose.JWK.createKeyStore();

        this.httpClient = axios.create({
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });

        this.energyTypesCache = {}
        this.vehicleModels = {}
    }

    async run() {
        await this.__getToken()
        await this.__loadEnergy()
        await this.__loadVehicleModels()
    }


    // Login POST request for retrieve JWT Token
    async __createSession(jws) {
        const url = `${this.endpoint}/login`
        return this.httpClient.post( url, {content: jws} , { headers: {'Content-Type': 'application/json'} } )
        .then( res => {return res.data.token;} )
        .catch( error => { console.error( 'On on POST login '+`${url}`, error.message ); throw error.message;} );
    }

    async __getToken() {
        if (!this.token) {

            this.token = await this.__loadToken()

        }
        return this.token
    }

    async __loadToken() {

        const tokenFileName = `${this.kid}.token`

        let token
        try {

            const readToken = await ( async () => {
                return new Promise( (resolve, reject) => {

                    fs.readFile(tokenFileName, 'utf8' , (err, data) => {
                        if (err) {
                        //console.error(err)
                        return reject(err)
                        }
                        resolve(data)
                    })
                })
            })()
            token = readToken
        } catch (err) {
        }
        if (!token) {
            // Read file with private key
            const privateKey = fs.readFileSync(`${this.kid}.pk`)
            console.log('Private key loaded with success')
            // Load key in JWS Keystore
            await this.keystore.add(privateKey, 'pem', {kid: this.kid})
            
            // Generate Signed JWS content
            const payload = {login: this.login, ts: new Date().getTime() / 1000}
            const jws = await this.__signPayload(JSON.stringify(payload))
            console.log('JWS Payload generated with success : ', jws)

            // Login with jws and retrieve jwt token for a session
            token = await this.__createSession(jws)    
            console.log('Session JWT Token retreived with success : ', token)

            fs.writeFile(tokenFileName, token, err => {
                if (err) {
                console.error(err)
                return
                }
                //file written successfully
            })          
        }
        return token
    }

    /**
     * Generate JWS signed content from JSON string Payload
     * @param {*} payload 
     */
    async __signPayload(payload) {

        const key = this.key ? this.key : this.keystore.get(this.kid);
        
        if (key) {
            this.key = key
            return jose.JWS.createSign({ format: 'compact' }, key).
                update(payload).
                final()
        } else {
            return Promise.reject();
        }
    }

    __getHauthHeaders() {
        return {'Content-Type': 'application/json', 'Authorization': `bearer ${this.token}`}
    }

    async stations() {
        return this.__authenticatedGet(`/station`)
    }

    async getStation( id ) {
        return this.__authenticatedGet(`/station/${id}`)
    }
    
    async getDispenser( id ) {
        return this.__authenticatedGet(`/dispenser/${id}`)
    }
 
    async createVehicle( name, model, fleet, badges ) {

        const payload = {
            name: name,
            modele: model,
            fleet,
            badges
        }

        return await this.__authenticatedPost(`/vehicle`, payload)


    }
    
    async createBadge( badgeId, fleet, vehicle ) {

        const payload = {
            badgeId,
            fleet,
            vehicle
        }

        return await this.__authenticatedPost(`/badge`, payload)

    }
    
    async updateBadge( id, enabled ) {

        const payload = {
            enabled
        }

        return await this.__authenticatedPatch(`/badge/${id}`, payload)

    }

    async createCustomer( name, fleetName, contractName, externalRefId, energies ) {

        const allowedEnergies = []

        energies.forEach( energyName => {
            const data = this.energyTypesCache['H2_' + energyName]
            if (data) {
                allowedEnergies.push(data)
            }
        });

        const payload = {
            name: name,
            fleets: [
                {
                    name: fleetName,
                    contracts: [
                      { name: contractName, externalRefId, start: new Date(), energies: allowedEnergies }
                    ],
                },
            ]
        }

        return await this.__authenticatedPost(`/customer`, payload)

    }

    async changeCustomerStatus( customer, status ) {
        let path
        switch (status) {
            case 2: { // ready
                path = 'customervalidate'
                break
            }
            case 3: { // locked
                path = 'customerlock'
                break
            }
            case 4: { // unlocked
                path = 'customerunlock'
                break
            }
            case 0: { // closed
                path = 'customerclose'
                break
            }
        }

        return await this.__authenticatedPost(`/customer/${customer.id}/${path}`, {})
    }

    async getCustomer( id ) {
        const result = await this.__authenticatedGet(`/customer/${id}`)
        return result
    }
    
    async getFleet( id ) {
        const result = await this.__authenticatedGet(`/fleet/${id}`)
        return result
    }

    async getFleets(customer) {
        const result = await this.__authenticatedGet(`/customer/${customer.id}/fleets`)
        return result.fleets
    }

    async activateContract( id ) {
        const path = 'activate'
        return await this.__authenticatedPost(`/customerContract/${id}/${path}`, {})
    }

    async addDriver( firstName, lastName, fleet ) {

        const driver = await this.__authenticatedPost(`/driver`, {
            firstName,
            lastName,
            fleet
        })

        return driver
    }

    async unlock(dispenser, customer, driver) {
        
        const path = 'authorizeunlock'
        return await this.__authenticatedPost(`/dispenser/${dispenser.id}/${path}`, {
            authorization: {
                customer,
                associatedDriver: driver,
                contactInfos: {
                    reference: '123456',
                    firstName: 'prenom',
                    lastName: 'nom'
                }
            }
        })
    }
    
    async getAuthorization( id ) {
        const result = await this.__authenticatedGet(`/authorization/${id}`)
        return result
    }
    
    async getRefills( from, to ) {
        const date = moment(from).format('YYYY-MM-DD')
        const result = await this.__authenticatedGet(`/refill?stationCreatedAt=${date}`)
        return result
    }

    async __loadEnergy() {
        const energies = await this.__authenticatedGet('/energy')
        console.log(energies)
        
        for (const energy of energies.data) {
            const energyTypes = await this.__authenticatedGet(`/energy/${energy.id}/types`)
            console.log(energyTypes)
            for (const energyType of energyTypes.types) {
                this.energyTypesCache[`${energy.name}_${energyType.name}`] = energyType
            }
        }
    }
    
    async __loadVehicleModels() {
        const vehicleModels = await this.__authenticatedGet('/vehicleModele')
        vehicleModels.data.forEach( model => {
            this.vehicleModels[model.key] = model
        })
    }

    // Generic HTTP Getter
    async __authenticatedGet(path) {
        // Send JWT Token in bearer header Authorization
        const url = `${this.endpoint}${path}`
        console.log('__authenticatedGet '+url)
        return this.httpClient.get(url, { headers: this.__getHauthHeaders() } )
        .then( res => {return res.data;} )
        .catch( error => {console.error( 'On on GET '+url, error.message ); throw error;} );
    }

    // Generic HTTP POST
    async __authenticatedPost(path, payload) {
        // Send JWT Token in bearer header Authorization
        const url = `${this.endpoint}${path}`
        console.log('__authenticatedPost '+url)
        return this.httpClient.post(url, payload, { headers: this.__getHauthHeaders() } )
        .then( res => {return res.data;} )
        .catch( error => {console.error( 'On on POST '+url, error.message ); throw error;} );
    }

    // Generic HTTP PUT
    async __authenticatedPatch(path, payload) {
        const url = `${this.endpoint}${path}`
        // Send JWT Token in bearer header Authorization
        console.log('__authenticatedPatch '+url)
        return this.httpClient.patch(url, payload, { headers: this.__getHauthHeaders() } )
        .then( res => {return res.data;} )
        .catch( error => {console.error( 'On on PATCH '+url, error.message ); throw error;} );
    }

}

module.exports = {
    FillnDriveClient
}