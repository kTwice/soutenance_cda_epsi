# FillnDrive client api example

This project is a nodejs example of the FillnDrive client API that show how to login and make a call for retrieving a list of refillgroup

## requirements

- Get your private key from the FillnDrive manager portal (file <KID>.pk)
- install nodejs on your system

## configuration

- Place your private key file (named in <KID>.pk) at the root folder of the project
- Launch cmd : npm install

## Launch

Launch cmd : KID=<KID> LOGIN=<USER LOGIN> node demo.js
            KID=cdf186e0-edfc-44ca-823e-f7741d7838f8 LOGIN='julie.fillatre@ekolis-eu.com' node demo.js

## Session expired

If your session is expired (401 errors), delete the file <KID>.token