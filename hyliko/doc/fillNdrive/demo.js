const {FillnDriveClient} = require('./client');



// Api Endpoint
const ENDPOINT = process.env.ENDPOINT ? process.env.ENDPOINT : 'https://demo-api.fndtech.ovh';

// JWS KID of private key
const KID = process.env.KID;

// Authorized user login
const LOGIN = process.env.LOGIN;


( async () => {

    // Initialisation
    const client = new FillnDriveClient(ENDPOINT, KID, LOGIN)
    await client.run()

    // Get station list
    const stations = await client.stations()
    stations.data.forEach( async s => {
        // Get each station details
        const fullStation = await client.getStation(s.id)
        console.log('===== STATION : '+ fullStation.name+' => '+fullStation.publicState+' (gps: latitude='+fullStation.gps.latitude+', longitude='+fullStation.gps.longitude+')')
        for (const dispenser of fullStation.dispensers) {
            console.log('===== DISPENSER '+ dispenser.name+' => '+dispenser.publicState)
            dispenser.receptacles.forEach( nozzle => {
                console.log('===== NOZZLE ' + nozzle.energyType?.name + ' => ' + nozzle.state )
            })

            const fullDispenser = await client.getDispenser(dispenser.id)
            
            if (fullDispenser.refiller && fullDispenser.refiller.authorization && fullDispenser.refiller.attempt) {

                console.log('============= RUNNING REFUELING =============')
                const authorization = fullDispenser.refiller.attempt
                const attempt = fullDispenser.refiller.attempt
                console.log('customer:'+authorization.customer?.id+' driver:'+authorization.associatedDriver?.id)
                console.log('attempt: ' + attempt.state+'/'+attempt.energyType+'/'+(attempt.quantity / 1000)+'kg')
                console.log('=============================================')
            }

        }
    } )
    
}) ()