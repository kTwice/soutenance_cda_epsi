# FillnDrive client api JAVA example

This project is a java example of the FillnDrive client API that show how to login and retrieve a token

## requirements

- Get your private key from the FillnDrive manager portal (file <KID>.pk)
- install java and maven on your system

## configuration

- Place your private key file (named in <KID>.pk) at the root folder of the project
- Replace the value of KID var in JWSExample.java with your KID
- Replace the value of LOGIN var in JWSExample.java with the email address of the user attached to the key