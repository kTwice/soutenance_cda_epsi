import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Calendar;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;

public class JWSExample {

    private static String ENDPOINT = "https://demo-api.fndtech.ovh/";
    private static String KID = "cdf186e0-edfc-44ca-823e-f7741d7838f8";
    private static String LOGIN = "julie.fillatre@ekolis-eu.com";

    public static void main(String[] args) throws Exception {

        if (KID == null || LOGIN == null) {
            throw new RuntimeException("Please set KID and LOGIN vars");
        }

        try {

            // Load private key from local file <kid>.pk
            PrivateKey pk = generatePrivateKey(KID);

            // Create RSA Signer with private key
            JWSSigner signer = new RSASSASigner(pk);

            // Create login payload with timestamp
            long ts = Calendar.getInstance().getTimeInMillis() / 1000;
            String message = "{\"login\": \"" + LOGIN + "\", \"ts\": " + ts + "}";
            Payload payload = new Payload(message);

            System.out.println("JWS payload message: " + message);

            // Create JWS header with RS256 algorithm
            JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                    .keyID(KID)
                    .build();

            // Create JWS object
            JWSObject jwsObject = new JWSObject(header, payload);

            jwsObject.sign(signer);
            String signedJws = jwsObject.serialize();

            System.out.println("Serialised JWS object: " + signedJws);

            // Send signed jws payload to login endpoint to retrieve a token
            httpPost("login", "{\"content\": \"" + signedJws + "\"}");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static PrivateKey generatePrivateKey(String kid)
            throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(kid + ".pk"));
        String PRIVATE_KEY = null;

        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            PRIVATE_KEY = sb.toString();
        } finally {
            br.close();
        }

        if (PRIVATE_KEY == null) {
            return null;
        }

        StringBuilder pkcs8Lines = new StringBuilder();
        BufferedReader rdr = new BufferedReader(new StringReader(PRIVATE_KEY));
        String line;
        while ((line = rdr.readLine()) != null) {
            pkcs8Lines.append(line);
        }

        // Remove the "BEGIN" and "END" lines, as well as any whitespace

        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

        // Base64 decode the result

        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);

        // extract the private key

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privKey = kf.generatePrivate(keySpec);

        return privKey;
    }

    static private void httpPost(String path, String payload) throws IOException, URISyntaxException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpUriRequest httppost = RequestBuilder.post()
                    .setUri(new URI(ENDPOINT + path))
                    .setEntity(new StringEntity(payload))
                    .build();

            httppost.setHeader("Accept", "application/json");
            httppost.addHeader("Content-Type", "application/json");

            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                System.out.println("TOKEN RESPONSE : " + EntityUtils.toString(response.getEntity()));
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }

}
